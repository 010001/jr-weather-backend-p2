const axios  = require("axios")
const openweathermapConfig = require('../config/openweathermap')

exports.getWeather = () =>{
    return axios.get(`http://api.openweathermap.org/data/2.5/forecast?lat=35&lon=139&appid=d124156a6891227117a47c8c985555e7`)
}
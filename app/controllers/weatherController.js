const weatherapiService = require('../services/openweathermapServiceAPI')

//GET ALL weather
exports.index = async (req,res) => {
    const data = await weatherapiService.getWeather();
    res.send(data.data);
}

//GET ONE get one city weather
exports.show = (req,res) => {

} 

//POST create weather
exports.store = (req,res) => {

}

//PUT update weather
exports.update = (req,res) => {

} 

//DELETE delete weather
exports.delete = (req,res) => {

}